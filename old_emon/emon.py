
import time

from minimalmodbus import Instrument, MODE_RTU, TIMEOUT
from brio.utils.dict import dotdict

from paho.mqtt.client import Client
import json

#import serial

# Reset Energy : f8 42 c2 41

TIMEOUT = 3.0

#ser = serial.Serial(port='/dev/ttyUSB0')

def scale_val(regs, scaling=1.0):
    if len(regs) == 1:
        return regs[0] * scaling
    elif len(regs) == 2:
        return (regs[0] | regs[1] << 8) * scaling



READINGS = dotdict({
    'V': {
        'args': {
            'registeraddress': 0,
            'numberOfRegisters': 1,
            'functioncode': 4
        },
        'scaling': 0.1,
        'units': ' V',
        'format': '{:.1f}'
    },
    'I': {
        'args': {
            'registeraddress': 1,
            'numberOfRegisters': 2,
            'functioncode': 4
        },
        'scaling': 0.001,
        'units': ' A',
        'format': '{:.3f}'
    },
    'P': {
        'args': {
            'registeraddress': 3,
            'numberOfRegisters': 2,
            'functioncode': 4
        },
        'scaling': 0.1,
        'units': ' W',
        'format': '{:.1f}'
    },
    'E': {
        'args': {
            'registeraddress': 5,
            'numberOfRegisters': 2,
            'functioncode': 4
        },
        'scaling': 1,
        'units': ' Wh',
        'format': '{:d}'
    },
    'F': {
        'args': {
            'registeraddress': 7,
            'numberOfRegisters': 1,
            'functioncode': 4
        },
        'scaling': 0.1,
        'units': ' Hz',
        'format': '{:.1f}'
    },
    'PF': {
        'args': {
            'registeraddress': 8,
            'numberOfRegisters': 1,
            'functioncode': 4
        },
        'scaling': 0.01,
        'units': '',
        'format': '{:.2f}'
    }
})


i = Instrument('/dev/ttyUSB0', 0xaa, MODE_RTU)
#i.debug = True

c = Client()

c.connect('127.0.0.1')

c.publish('emon', 'Hello!')



while True:
    h = {}
    r = {}
    for k, v in READINGS.items():
        regs = i.read_registers(**v.args)
        val = scale_val(regs, v.scaling)
        h.update({k: v.format.format(val) + v.units})
        r.update({k: v.format.format(val)})
        c.publish('emon/d/{}'.format(k), val)
    print(json.dumps(h))
    c.publish('emon/l', json.dumps(h))
    c.publish('emon/r', json.dumps(r))

